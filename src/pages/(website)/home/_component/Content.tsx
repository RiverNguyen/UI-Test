import { ArrowRight } from "lucide-react";
import { Link } from "react-router-dom";

const Content = () => {
    return (
        <>
            <div className="grid md:grid-cols-1 lg:grid-cols-2 gap-0 w-full">
                <div className="">
                    <img src="src/assets/images/4.png" alt="" />
                </div>
                <div className="bg-[#F3F5F7] flex items-center md:py-16 lg:py-0 p-[32px]">
                    <div className="ml-0 sm:ml-14">
                        <h1 className="font-bold text-[#377DFF] mb-3">
                            SALE UP TO 35% OFF
                        </h1>
                        <h1 className="font-medium text-[34px] sm:text-[48px] leading-tight mb-3 tracking-wider">
                            HUNDREDS of <br className="sm:block hidden" /> New
                            lower prices!
                        </h1>
                        <p className="sm:text-[20px] mb-6 tracking-wider">
                            It’s more affordable than ever to give every{" "}
                            <br className="sm:block hidden" /> room in your home
                            a stylish makeover
                        </p>
                        <div className="underline flex items-center transition duration-500 ease-in-out transform hover:translate-x-2 hover:opacity-70 ">
                            <Link className="font-medium" to="">
                                Shop Now
                            </Link>
                            <ArrowRight className="ml-1 h-4 w-4" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Content;
