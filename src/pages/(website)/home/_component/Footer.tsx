import { Facebook, Instagram, Youtube } from "lucide-react";
import { Link, NavLink } from "react-router-dom";

const Footer = () => {
    return (
        <>
            <div className="flex sm:h-[250px] h-auto bg-[#232627] sm:bg-[#141718] text-white p-10 sm:p-0">
                <div className="container mx-auto flex flex-col justify-center">
                    <div className="flex sm:justify-between sm:flex-row flex-col items-center">
                        <div className="flex flex-col sm:flex-row items-center">
                            <div className="border border-b-[#6C7275] border-l-0 border-r-0 border-t-0 pb-3 sm:p-0 sm:border-none">
                                <img
                                    src="src/assets/images/3legant-1.svg"
                                    className="sm:w-[124px] sm:h-[24px] sm:pr-10 "
                                    alt=""
                                />
                            </div>
                            <p className="text-center sm:text-left md:pl-10 my-5 sm:my-0 md:border-l md:border-l-[#6C7275] md:border-r-0 md:border-t-0 md:border-b-0">
                                Gift & Decoration Store
                            </p>
                        </div>
                        <div className="*:transform flex sm:flex-row flex-col items-center">
                            <NavLink
                                className={"sm:mr-10 sm:mb-0 mb-5"}
                                to={""}
                            >
                                Home
                            </NavLink>
                            <NavLink
                                className={"sm:mr-10 sm:mb-0 mb-5"}
                                to={""}
                            >
                                Shop
                            </NavLink>
                            <NavLink
                                className={"sm:mr-10 sm:mb-0 mb-5"}
                                to={""}
                            >
                                Product
                            </NavLink>
                            <NavLink
                                className={"sm:mr-10 sm:mb-0 mb-5"}
                                to={""}
                            >
                                Blog
                            </NavLink>
                            <NavLink to={""}>Contact Us</NavLink>
                        </div>
                    </div>
                    <div className="flex justify-center items-center flex-col-reverse sm:flex-row sm:justify-between pt-3 border-t-[#6C7275] border border-b-0 border-l-0 border-r-0 mt-[40px]">
                        <div className="flex sm:*:mr-6 items-center mt-5 sm:mt-0">
                            <p className="text-[12px]">
                                Copyright © 2023 3legant. All rights reserved
                            </p>
                            <Link className="sm:block hidden" to={""}>
                                Privacy Policy
                            </Link>
                            <Link className="sm:block hidden" to={""}>
                                Terms of Use
                            </Link>
                        </div>
                        <div className="sm:hidden block mt-5">
                            <Link className="mr-6" to={""}>
                                Privacy Policy
                            </Link>
                            <Link className="" to={""}>
                                Terms of Use
                            </Link>
                        </div>
                        <div className="flex mt-5 sm:mt-0">
                            <Instagram className="mr-5" />
                            <Facebook className="mr-5" />
                            <Youtube className="mr-0" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Footer;
