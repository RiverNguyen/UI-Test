import { Fragment, useState } from "react";
import { Dialog, Disclosure, Popover, Transition } from "@headlessui/react";
import {
    ArrowPathIcon,
    Bars3Icon,
    ChartPieIcon,
    CursorArrowRaysIcon,
    FingerPrintIcon,
    SquaresPlusIcon,
    XMarkIcon,
} from "@heroicons/react/24/outline";
import {
    ChevronDownIcon,
    // PhoneIcon,
    // PlayCircleIcon,
} from "@heroicons/react/20/solid";
import { Link, NavLink } from "react-router-dom";
import {
    CircleUserRound,
    Facebook,
    Heart,
    Instagram,
    Minus,
    Plus,
    Search,
    ShoppingBag,
    X,
    Youtube,
} from "lucide-react";

const products = [
    {
        name: "Analytics",
        description: "Get a better understanding of your traffic",
        href: "#",
        icon: ChartPieIcon,
    },
    {
        name: "Engagement",
        description: "Speak directly to your customers",
        href: "#",
        icon: CursorArrowRaysIcon,
    },
    {
        name: "Security",
        description: "Your customers’ data will be safe and secure",
        href: "#",
        icon: FingerPrintIcon,
    },
    {
        name: "Integrations",
        description: "Connect with third-party tools",
        href: "#",
        icon: SquaresPlusIcon,
    },
    {
        name: "Automations",
        description: "Build strategic funnels that will convert",
        href: "#",
        icon: ArrowPathIcon,
    },
];
const cart = [
    {
        id: 1,
        name: "Throwback Hip Bag",
        href: "#",
        color: "Salmon",
        price: "$90.00",
        quantity: 1,
        imageSrc:
            "https://tailwindui.com/img/ecommerce-images/shopping-cart-page-04-product-01.jpg",
        imageAlt:
            "Salmon orange fabric pouch with match zipper, gray zipper pull, and adjustable hip belt.",
    },
    {
        id: 2,
        name: "Medium Stuff Satchel",
        href: "#",
        color: "Blue",
        price: "$32.00",
        quantity: 1,
        imageSrc:
            "https://tailwindui.com/img/ecommerce-images/shopping-cart-page-04-product-02.jpg",
        imageAlt:
            "Front of satchel with blue canvas body, black straps and handle, drawstring top, and front zipper pouch.",
    },
    // More products...
];

function classNames(...classes: string[]) {
    return classes.filter(Boolean).join(" ");
}

export default function Example() {
    const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
    const [open, setOpen] = useState(false);
    const [count, setCount] = useState(1);
    const setPlus = () => {
        setCount(count + 1);
    };
    const setMinus = () => {
        if (count > 1) {
            setCount(count - 1);
        }
    };

    return (
        <header className="bg-white">
            <nav
                className="mx-auto flex items-center justify-between py-6 "
                aria-label="Global"
            >
                <div className="flex lg:flex-1">
                    <div className="flex lg:hidden">
                        <button
                            type="button"
                            className="-m-2.5 mr-1 inline-flex justify-center rounded-md p-2.5 text-gray-700"
                            onClick={() => setMobileMenuOpen(true)}
                        >
                            <span className="sr-only">Open main menu</span>
                            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                        </button>
                    </div>
                    <a href="#" className="-m-1.5 p-1.5">
                        <span className="sr-only">Your Company</span>
                        <img
                            className="h-8 w-auto"
                            src="src/assets/images/3legant.svg"
                            alt=""
                        />
                    </a>
                </div>
                <div className="flex items-center lg:hidden">
                    <button
                        className="flex items-center"
                        onClick={() => setOpen(true)}
                    >
                        <ShoppingBag className="mr-[10px]" />
                        <div className="rounded-full bg-black text-white w-[20px] h-[20px] text-center leading-[20px]">
                            2
                        </div>
                    </button>
                </div>

                <Popover.Group className="hidden lg:flex lg:gap-x-12">
                    <NavLink
                        to=""
                        className="text-sm font-semibold leading-6 text-gray-900"
                    >
                        Home
                    </NavLink>
                    <NavLink
                        to=""
                        className="text-sm font-semibold leading-6 text-gray-900"
                    >
                        Shop
                    </NavLink>
                    <Popover className="relative">
                        <Popover.Button className="flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900">
                            Product
                            <ChevronDownIcon
                                className="h-5 w-5 flex-none text-gray-400"
                                aria-hidden="true"
                            />
                        </Popover.Button>

                        <Transition
                            as={Fragment}
                            enter="transition ease-out duration-200"
                            enterFrom="opacity-0 translate-y-1"
                            enterTo="opacity-100 translate-y-0"
                            leave="transition ease-in duration-150"
                            leaveFrom="opacity-100 translate-y-0"
                            leaveTo="opacity-0 translate-y-1"
                        >
                            <Popover.Panel className="absolute -left-8 top-full z-10 mt-3 w-screen max-w-md overflow-hidden rounded-3xl bg-white shadow-lg ring-1 ring-gray-900/5">
                                <div className="p-4">
                                    {products.map((item) => (
                                        <div
                                            key={item.name}
                                            className="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50"
                                        >
                                            <div className="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                                <item.icon
                                                    className="h-6 w-6 text-gray-600 group-hover:text-indigo-600"
                                                    aria-hidden="true"
                                                />
                                            </div>
                                            <div className="flex-auto">
                                                <a
                                                    href={item.href}
                                                    className="block font-semibold text-gray-900"
                                                >
                                                    {item.name}
                                                    <span className="absolute inset-0" />
                                                </a>
                                                <p className="mt-1 text-gray-600">
                                                    {item.description}
                                                </p>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                                {/* <div className="grid grid-cols-2 divide-x divide-gray-900/5 bg-gray-50">
                                    {callsToAction.map((item) => (
                                        <a
                                            key={item.name}
                                            href={item.href}
                                            className="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100"
                                        >
                                            <item.icon
                                                className="h-5 w-5 flex-none text-gray-400"
                                                aria-hidden="true"
                                            />
                                            {item.name}
                                        </a>
                                    ))}
                                </div> */}
                            </Popover.Panel>
                        </Transition>
                    </Popover>

                    <NavLink
                        to=""
                        className="text-sm font-semibold leading-6 text-gray-900"
                    >
                        Contact Us
                    </NavLink>
                </Popover.Group>
                <div className="hidden lg:flex lg:flex-1 lg:justify-end lg:items-center">
                    <Search className="mr-[20px]" />
                    <CircleUserRound className="mr-[20px]" />
                    <button
                        className="flex items-center"
                        onClick={() => setOpen(true)}
                    >
                        <ShoppingBag className="mr-[10px]" />
                        <div className="rounded-full bg-black text-white w-[20px] h-[20px] text-center leading-[20px]">
                            2
                        </div>
                    </button>
                </div>
            </nav>
            <Transition.Root show={mobileMenuOpen} as={Fragment}>
                <Dialog
                    className="lg:hidden"
                    open={mobileMenuOpen}
                    onClose={setMobileMenuOpen}
                >
                    <Transition.Child
                        as={Fragment}
                        enter="ease-in-out duration-500"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in-out duration-500"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    <Transition.Child
                        as={Fragment}
                        enter="transform transition ease-in-out duration-500 sm:duration-700"
                        enterFrom="-translate-x-full"
                        enterTo="translate-x-0"
                        leave="transform transition ease-in-out duration-500 sm:duration-700"
                        leaveFrom="translate-x-0"
                        leaveTo="-translate-x-full"
                    >
                        <Dialog.Panel className="fixed flex flex-col justify-between inset-y-0 left-0 z-10 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10 rounded-lg">
                            <div className="">
                                <div className="flex items-center justify-between">
                                    <a href="#" className="-m-1.5 p-1.5">
                                        <span className="sr-only">
                                            Your Company
                                        </span>
                                        <img
                                            className="h-5 w-auto"
                                            src="src/assets/images/3legant.svg"
                                            alt=""
                                        />
                                    </a>
                                    <button
                                        type="button"
                                        className="-m-2.5 rounded-md p-2.5 text-gray-700 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110"
                                        onClick={() => setMobileMenuOpen(false)}
                                    >
                                        <span className="sr-only">
                                            Close menu
                                        </span>
                                        <XMarkIcon
                                            className="h-6 w-6"
                                            aria-hidden="true"
                                        />
                                    </button>
                                </div>
                                <div className="mt-6 flow-root">
                                    <div className="-my-6">
                                        <div className="space-y-2 py-6 ">
                                            <div className="flex items-center border px-3.5 py-2 rounded-lg">
                                                <Search strokeWidth={1} />

                                                <input
                                                    id="search"
                                                    name="search"
                                                    type="text"
                                                    className="w-auto flex-auto rounded-md border-0 pl-2 sm:text-sm sm:leading-6 focus:outline-none"
                                                    placeholder="Search..."
                                                />
                                            </div>
                                            <a
                                                href="#"
                                                className="-mx-3 block px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                            >
                                                Home
                                            </a>
                                            <div className="border-b"></div>
                                            <Disclosure
                                                as="div"
                                                className="-mx-3"
                                            >
                                                {({ open }) => (
                                                    <>
                                                        <Disclosure.Button className="flex w-full items-center justify-between rounded-lg py-2 pl-3 pr-3.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">
                                                            Product
                                                            <ChevronDownIcon
                                                                className={classNames(
                                                                    open
                                                                        ? "rotate-180"
                                                                        : "",
                                                                    "h-5 w-5 flex-none"
                                                                )}
                                                                aria-hidden="true"
                                                            />
                                                        </Disclosure.Button>
                                                        <Disclosure.Panel className="mt-2 space-y-2">
                                                            {[...products].map(
                                                                (item) => (
                                                                    <Disclosure.Button
                                                                        key={
                                                                            item.name
                                                                        }
                                                                        as="a"
                                                                        href={
                                                                            item.href
                                                                        }
                                                                        className="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                                                    >
                                                                        {
                                                                            item.name
                                                                        }
                                                                    </Disclosure.Button>
                                                                )
                                                            )}
                                                        </Disclosure.Panel>
                                                    </>
                                                )}
                                            </Disclosure>
                                            <div className="border-b"></div>
                                            <Disclosure
                                                as="div"
                                                className="-mx-3"
                                            >
                                                {({ open }) => (
                                                    <>
                                                        <Disclosure.Button className="flex w-full items-center justify-between rounded-lg py-2 pl-3 pr-3.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">
                                                            Shop
                                                            <ChevronDownIcon
                                                                className={classNames(
                                                                    open
                                                                        ? "rotate-180"
                                                                        : "",
                                                                    "h-5 w-5 flex-none"
                                                                )}
                                                                aria-hidden="true"
                                                            />
                                                        </Disclosure.Button>
                                                        <Disclosure.Panel className="mt-2 space-y-2">
                                                            {[...products].map(
                                                                (item) => (
                                                                    <Disclosure.Button
                                                                        key={
                                                                            item.name
                                                                        }
                                                                        as="a"
                                                                        href={
                                                                            item.href
                                                                        }
                                                                        className="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                                                    >
                                                                        {
                                                                            item.name
                                                                        }
                                                                    </Disclosure.Button>
                                                                )
                                                            )}
                                                        </Disclosure.Panel>
                                                    </>
                                                )}
                                            </Disclosure>
                                            <div className="border-b"></div>

                                            <a
                                                href="#"
                                                className="-mx-3 block px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                            >
                                                Contact Us
                                            </a>
                                            <div className="border-b"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="">
                                <div className="mt-6 flow-root">
                                    <div className="-my-6">
                                        <div className="py-6 flex justify-between items-center">
                                            <a
                                                href="#"
                                                className="-mx-3 block px-3 py-2 text-base font-semibold leading-7 text-[#6C7275] hover:bg-gray-50"
                                            >
                                                Cart
                                            </a>
                                            <div className="flex items-center lg:hidden">
                                                <ShoppingBag className="mr-[6px]" />
                                                <div className="rounded-full bg-black text-white w-[20px] h-[20px] text-center leading-[20px]">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="border-b mt-1"></div>
                                    <div className="-my-6 ">
                                        <div className="mt-1 py-6 flex justify-between items-center">
                                            <a
                                                href="#"
                                                className="-mx-3 block px-3 py-2 text-base font-semibold leading-7 text-[#6C7275] hover:bg-gray-50"
                                            >
                                                Wishlist
                                            </a>
                                            <div className="flex items-center lg:hidden">
                                                <Heart className="mr-[6px]" />
                                                <div className="rounded-full bg-black text-white w-[20px] h-[20px] text-center leading-[20px]">
                                                    2
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="border-b mt-1"></div>
                                    <button className="mx-auto bg-black w-full mt-5 h-[52px] rounded-lg text-white">
                                        Sign In
                                    </button>
                                    <div className="flex mt-3">
                                        <Instagram className="mr-2" />
                                        <Facebook className="mr-2" />
                                        <Youtube />
                                    </div>
                                </div>
                            </div>
                        </Dialog.Panel>
                    </Transition.Child>
                </Dialog>
            </Transition.Root>
            <Transition.Root show={open} as={Fragment}>
                <Dialog className="relative z-10" onClose={setOpen}>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-in-out duration-500"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in-out duration-500"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    <div className="fixed inset-0 overflow-hidden">
                        <div className="absolute inset-0 overflow-hidden">
                            <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
                                <Transition.Child
                                    as={Fragment}
                                    enter="transform transition ease-in-out duration-500 sm:duration-700"
                                    enterFrom="translate-x-full"
                                    enterTo="translate-x-0"
                                    leave="transform transition ease-in-out duration-500 sm:duration-700"
                                    leaveFrom="translate-x-0"
                                    leaveTo="translate-x-full"
                                >
                                    <Dialog.Panel className="pointer-events-auto w-screen max-w-md">
                                        <div className="flex h-full flex-col overflow-y-scroll bg-white shadow-xl">
                                            <div className="flex-1 overflow-y-auto px-4 py-6 sm:px-6">
                                                <div className="flex items-start justify-between">
                                                    <Dialog.Title className="text-[28px] font-medium text-gray-900">
                                                        Cart
                                                    </Dialog.Title>
                                                    <div className="ml-3 flex h-7 items-center">
                                                        <button
                                                            type="button"
                                                            className="relative -m-2 p-2 text-gray-400 hover:text-gray-500"
                                                            onClick={() =>
                                                                setOpen(false)
                                                            }
                                                        >
                                                            <span className="absolute -inset-0.5" />
                                                            <span className="sr-only">
                                                                Close panel
                                                            </span>
                                                            <XMarkIcon
                                                                className="h-6 w-6"
                                                                aria-hidden="true"
                                                            />
                                                        </button>
                                                    </div>
                                                </div>

                                                <div className="mt-8">
                                                    <div className="flow-root">
                                                        <ul
                                                            role="list"
                                                            className="-my-6 divide-y divide-gray-200"
                                                        >
                                                            {cart.map(
                                                                (item) => (
                                                                    <li
                                                                        key={
                                                                            item.id
                                                                        }
                                                                        className="flex py-6"
                                                                    >
                                                                        <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                                                                            <img
                                                                                src={
                                                                                    item.imageSrc
                                                                                }
                                                                                alt={
                                                                                    item.imageAlt
                                                                                }
                                                                                className="h-full w-full object-cover object-center"
                                                                            />
                                                                        </div>

                                                                        <div className="ml-4 flex flex-1 flex-col">
                                                                            <div>
                                                                                <div className="flex justify-between text-base font-medium text-gray-900">
                                                                                    <h3>
                                                                                        <a
                                                                                            href={
                                                                                                item.href
                                                                                            }
                                                                                        >
                                                                                            {
                                                                                                item.name
                                                                                            }
                                                                                        </a>
                                                                                    </h3>
                                                                                    <p className="ml-4">
                                                                                        {
                                                                                            item.price
                                                                                        }
                                                                                    </p>
                                                                                </div>
                                                                                <div className="flex flex-1 justify-between items-center mt-1">
                                                                                    <p className="mt-1 text-sm text-gray-500">
                                                                                        {
                                                                                            item.color
                                                                                        }
                                                                                    </p>
                                                                                    <div className="flex">
                                                                                        <button
                                                                                            type="button"
                                                                                            className="font-medium "
                                                                                        >
                                                                                            <X />
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div className="flex flex-1 items-end justify-between text-sm">
                                                                                <div className="flex items-center border border-black rounded-lg py-1.5 px-2 mt-2">
                                                                                    <button>
                                                                                        <Minus
                                                                                            className="h-4 w-4"
                                                                                            strokeWidth={
                                                                                                1
                                                                                            }
                                                                                            onClick={() =>
                                                                                                setMinus()
                                                                                            }
                                                                                        />
                                                                                    </button>
                                                                                    <span className="mx-3">
                                                                                        {
                                                                                            count
                                                                                        }
                                                                                    </span>
                                                                                    <button
                                                                                        onClick={() =>
                                                                                            setPlus()
                                                                                        }
                                                                                    >
                                                                                        <Plus
                                                                                            className="h-4 w-4"
                                                                                            strokeWidth={
                                                                                                1
                                                                                            }
                                                                                            onClick={() =>
                                                                                                setPlus()
                                                                                            }
                                                                                        />
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                )
                                                            )}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="border-b border-gray-200 px-4 py-6 sm:px-6">
                                                <div className="flex justify-between text-base text-gray-900">
                                                    <p>Subtotal</p>
                                                    <p>$262.00</p>
                                                </div>
                                            </div>

                                            <div className=" border-gray-200 px-4 py-6 sm:px-6">
                                                <div className="flex justify-between  font-medium text-[20px] text-gray-900">
                                                    <p>Total</p>
                                                    <p>$262.00</p>
                                                </div>

                                                <div className="mt-6">
                                                    <a
                                                        href="#"
                                                        className="flex items-center justify-center rounded-md border border-transparent bg-black px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-gray-900"
                                                    >
                                                        Checkout
                                                    </a>
                                                </div>
                                                <div className="mt-6 flex justify-center text-center text-sm text-gray-500">
                                                    <Link
                                                        to=""
                                                        className="mr-3 text-black hover:text-gray-900 underline font-bold text-[14px]"
                                                    >
                                                        View Cart{" "}
                                                    </Link>
                                                    <p>
                                                        or{" "}
                                                        <button
                                                            type="button"
                                                            className="font-medium text-black hover:text-gray-900"
                                                            onClick={() =>
                                                                setOpen(false)
                                                            }
                                                        >
                                                            Continue Shopping
                                                            <span aria-hidden="true">
                                                                {" "}
                                                                &rarr;
                                                            </span>
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </Dialog.Panel>
                                </Transition.Child>
                            </div>
                        </div>
                    </div>
                </Dialog>
            </Transition.Root>
        </header>
    );
}
