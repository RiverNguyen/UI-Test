// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
// import required modules
import { ArrowRight } from "lucide-react";
import { Link } from "react-router-dom";
import { Autoplay, EffectFade, Navigation, Pagination } from "swiper/modules";

export default function Banner() {
    const pagination = {
        dynamicBullets: true,
        renderBullet: function (index: number, className: string) {
            return "<span class='" + className + "'>" + (index + 1) + "</span>";
        },
    };
    return (
        <>
            <div className="mx-auto container">
                <div className="">
                    <Swiper
                        spaceBetween={30}
                        effect={"fade"}
                        pagination={pagination}
                        loop={true}
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        modules={[EffectFade, Navigation, Pagination, Autoplay]}
                        className="mySwiper swiper-1 rounded-lg"
                    >
                        <SwiperSlide key={"slide1"}>
                            <img
                                src="src/assets/images/banner.png"
                                className="h-full object-cover"
                            />
                        </SwiperSlide>
                        <SwiperSlide key={"slide2"}>
                            <img
                                src="src/assets/images/banner-1.png"
                                className="h-full object-cover"
                            />
                        </SwiperSlide>
                    </Swiper>
                </div>
                <div className="lg:grid lg:grid-cols-2 lg:gap-3 my-4 flex-col text-center">
                    <div className="font-medium text-[72px]">
                        <h1>Simply Unique/</h1>
                        <h1>Simply Better.</h1>
                    </div>
                    <div className="text-[#6C7275] lg:m-auto tracking-wider my-5">
                        <p>
                            <b className="text-black">3legant</b> is a gift &
                            decorations store based in HCMC, <br /> Vietnam. Est
                            since 2019.
                        </p>
                    </div>
                </div>
                <div className="grid grid-cols-1 lg:grid-cols-2 lg:grid-flow-col gap-7 *:bg-[#F3F5F7]">
                    <div className="row-span-3 h-[784px] relative rounded-xl">
                        <div className="absolute top-[48px] left-[48px] ">
                            <h1 className="text-[34px] font-medium ">
                                Living Room
                            </h1>
                            <Link
                                className="underline flex items-center font-medium transition duration-500 ease-in-out transform hover:translate-x-2 hover:opacity-70 hover:scale-105"
                                to={""}
                            >
                                Shop now <ArrowRight className="h-4" />
                            </Link>
                        </div>
                        <img
                            className="w-[548px] h-[664px] mx-auto mt-[6rem] hover:scale-105 transition duration-500 ease-in-out transform cursor-pointer"
                            src="src/assets/images/Paste image.png"
                            alt=""
                        />
                    </div>
                    <div className="h-[378px] relative rounded-xl">
                        <div className="absolute bottom-[48px] left-[32px]">
                            <h1 className="text-[34px] font-medium">Bedroom</h1>
                            <Link
                                className="underline flex items-center font-medium transition duration-500 ease-in-out transform hover:translate-x-2 hover:opacity-70 hover:scale-105"
                                to={""}
                            >
                                Shop now <ArrowRight className="h-4" />
                            </Link>
                        </div>
                        <img
                            src="src/assets/images/1.png"
                            className="h-[332px] w-[260px] my-auto float-end mr-16 mt-6 hover:scale-105 transition duration-500 ease-in-out transform cursor-pointer"
                            alt=""
                        />
                    </div>
                    <div className="h-[378px] relative rounded-xl">
                        <div className="absolute bottom-[48px] left-[32px]">
                            <h1 className="text-[34px] font-medium">Kitchen</h1>
                            <Link
                                className="underline flex items-center font-medium transition duration-500 ease-in-out transform hover:translate-x-2 hover:opacity-70 hover:scale-105"
                                to={""}
                            >
                                Shop now <ArrowRight className="h-4" />
                            </Link>
                        </div>
                        <img
                            src="src/assets/images/2.png"
                            className="h-[260px] w-[280px] float-end mr-16 mt-16 hover:scale-105 transition duration-500 ease-in-out transform cursor-pointer"
                            alt=""
                        />
                    </div>
                </div>
            </div>
        </>
    );
}
