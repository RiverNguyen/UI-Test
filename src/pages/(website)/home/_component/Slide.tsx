// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
// import required modules
import { ArrowRight, Heart, Star, StarHalf } from "lucide-react";
import { Link } from "react-router-dom";
import { FreeMode, Scrollbar } from "swiper/modules";

const Slide = () => {
    return (
        <>
            <div className="mx-auto container px-[32px] sm:px-0">
                <div className="flex justify-between *:font-medium mb-12 mt-9">
                    <div className="text-[40px] leading-10">
                        <h1>
                            New <br /> Arrivals
                        </h1>
                    </div>
                    <div className="flex items-center transition duration-500 ease-in-out transform hover:-translate-x-1 hover:opacity-70">
                        <Link to={""} className="underline">
                            More Products
                        </Link>
                        <ArrowRight className="h-4" />
                    </div>
                </div>
                <Swiper
                    slidesPerView={1}
                    spaceBetween={30}
                    freeMode={true}
                    scrollbar={true}
                    modules={[FreeMode, Scrollbar]}
                    className="mySwiper h-[600px]"
                    breakpoints={{
                        640: {
                            slidesPerView: 1.8,
                        },
                        768: {
                            slidesPerView: 2,
                        },
                        1024: {
                            slidesPerView: 2.7,
                        },
                        1280: {
                            slidesPerView: 3.5,
                        },
                        1536: {
                            slidesPerView: 4,
                        },
                    }}
                >
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="bg-[#F3F5F7] rounded-xl relative group transition duration-500 ease-in-out hover:shadow-lg">
                            <Link to={""} className="cursor-pointer">
                                <img
                                    src="src/assets/images/3.png"
                                    alt=""
                                    className="transform scale-100 group-hover:scale-105 transition-transform duration-500 ease-in-out"
                                />
                            </Link>
                            <div className="absolute top-6 left-4 uppercase font-bold px-4 rounded-md bg-white">
                                New
                            </div>
                            <div className="absolute top-14 left-4 uppercase font-bold px-4 rounded-md bg-[#38CB89] text-white">
                                -50%
                            </div>
                            <Heart className="cursor-pointer absolute top-6 right-4 h-10 w-6 transition duration-500 ease-in-out transform hover:fill-red-500 hover:-translate-y-1 " />
                            <Link
                                to={""}
                                className="absolute cursor-pointer left-0 right-0 bottom-6 mx-auto bg-black text-white text-center w-[230px] p-[12px] rounded-lg transform translate-y-10 group-hover:translate-y-0 transition-all duration-500 ease-in-out opacity-0 group-hover:opacity-100"
                            >
                                Add to cart
                            </Link>
                        </div>
                        <div className="*:my-3">
                            <div className="star-rating relative">
                                <div className="stars flex *:h-5 *:w-5">
                                    {Array.from({ length: 5 }, () => (
                                        <Star />
                                    ))}
                                </div>
                                <div className="stars rating absolute top-0 flex *:h-5 *:w-5">
                                    <Star fill="black" strokeWidth={0} />
                                    <Star fill="black" strokeWidth={0} />
                                    <StarHalf fill="black" strokeWidth={0} />
                                </div>
                            </div>
                            <h1 className="font-semibold">Loveseat Sofa</h1>
                            <div className="flex *:text-[14px]">
                                <p className="mr-3 font-semibold">$199.00</p>
                                <p className="line-through text-[#6C7275]">
                                    $400.00
                                </p>
                            </div>
                        </div>
                    </SwiperSlide>
                </Swiper>
            </div>
        </>
    );
};

export default Slide;
