import { Banknote, LockKeyhole, Phone, Truck } from "lucide-react";

const Values = () => {
    return (
        <>
            <div className="mx-auto container">
                <div className="grid lg:grid-cols-4 grid-cols-2 gap-2 sm:gap-7 *:w-auto *:rounded-lg my-[74px] px-[32px] sm:px-0">
                    <div className="bg-[#F3F5F7] h-[200px] sm:h-[280px] flex justify-center flex-col">
                        <div className="ml-5 sm:ml-12">
                            <Truck
                                className="h-10 w-10 sm:h-14 sm:w-14 mb-3"
                                strokeWidth={1}
                            />
                            <h1 className="font-medium text-[16px] sm:text-[20px] tracking-wider my-3">
                                Free Shipping
                            </h1>
                            <p className="text-[14px] text-[#6C7275]">
                                Order above $200
                            </p>
                        </div>
                    </div>
                    <div className="bg-[#F3F5F7] h-[200px] sm:h-[280px] flex justify-center flex-col">
                        <div className="ml-5 sm:ml-12">
                            <Banknote
                                className="h-10 w-10 sm:h-14 sm:w-14 mb-3"
                                strokeWidth={1}
                            />
                            <h1 className="font-medium text-[16px] sm:text-[20px] tracking-wider my-3">
                                Money-back
                            </h1>
                            <p className="text-[14px] text-[#6C7275]">
                                30 days guarantee
                            </p>
                        </div>
                    </div>
                    <div className="bg-[#F3F5F7] h-[200px] sm:h-[280px] flex justify-center flex-col">
                        <div className="ml-5 sm:ml-12">
                            <LockKeyhole
                                className="h-10 w-10 sm:h-14 sm:w-14 mb-3"
                                strokeWidth={1}
                            />
                            <h1 className="font-medium text-[16px] sm:text-[20px] tracking-wider my-3">
                                Secure Payments
                            </h1>
                            <p className="text-[14px] text-[#6C7275]">
                                Secured by Stripe
                            </p>
                        </div>
                    </div>
                    <div className="bg-[#F3F5F7] h-[200px] sm:h-[280px] flex justify-center flex-col">
                        <div className="ml-5 sm:ml-12">
                            <Phone
                                className="h-10 w-10 sm:h-14 sm:w-14 mb-3"
                                strokeWidth={1}
                            />
                            <h1 className="font-medium text-[16px] sm:text-[20px] tracking-wider my-3">
                                24/7 Support
                            </h1>
                            <p className="text-[14px] text-[#6C7275]">
                                Phone and Email support
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Values;
